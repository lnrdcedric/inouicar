<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.jqueryui.min.css" />
    <link href="css/style.css" rel="stylesheet" type="text/css">    
    
   
<title>Vehicle list</title>
</head>
<body>

	<div class="header">
	<a href="<%=application.getContextPath()%>/index">
	<img src="./img/logoSmall.png" alt="inouicar" width="80" height="80"/>
	</a>
		<h1> Vehicle list </h1>
	
	</div>

	<table id="table" class="display"  style="width:100%">
	<thead>
	    <tr>
            <th>Vehicle ID</th>
            <th>Cetegory</th>
            <th>Brand</th>
            <th>Model</th>
            <th>Licence plate</th>
            <th>Status</th>
            <th>Insurance policy</th>
            <th>Standard daily rate</th>
            <th>Number of seats</th>
            <th>Delete</th>
        </tr>
        
		<tr>
			<td><c:out value="Vehicle Id" /></td>
			<td><c:out value="Vehicle fk_vehiclecategory_id" /></td>
			<td><c:out value="Vehicle Brand" /></td>
			<td><c:out value="Vehicle Model" /></td>
			<td><c:out value="Vehicle license plate" /></td>
			<td><c:out value="Vehicle status" /></td>
			<td><c:out value="Vehicle insurance policy" /></td>
			<td><c:out value="Vehicle standard daily rate" /></td>
			<td><c:out value="Vehicle number of seats" /></td>
			<td><c:out value="Update Vehicle in Database" /></td>
			<td><c:out value="Delete Vehicule From Database" /></td>
			
		</tr>
		</thead>
		<tbody>
		<c:forEach var="Entry" items="${Vehicle_List}">
			<tr>
		
				<td><c:out value="${Entry.key}" /></td>
				<td><c:out value="${Entry.value.fk_vehiclecategory_id}" /></td>
				<td><c:out value="${Entry.value.vehicle_brand}" /></td>
				<td><c:out value="${Entry.value.vehicle_model}" /></td>
				<td><c:out value="${Entry.value.vehicle_licenceplate}" /></td>
				<td><c:out value="${Entry.value.vehicle_status}" /></td>
				<td><c:out value="${Entry.value.vehicle_insurancepolicy}" /></td>
				<td><c:out value="${Entry.value.vehicle_standarddailyrate}" /></td>
				<td><c:out value="${Entry.value.vehicle_numberofseats}" /></td>
				<td><a href="<%=application.getContextPath()%>/modifyVehicle?vehicle_id=${Entry.key}">Update</a></td>
				<td><a href="<%=application.getContextPath()%>/vehicleList?vehicleDel_id=${Entry.key}" onclick="return confirm('Are you sure you want to delete the ${Entry.value.vehicle_brand} ${Entry.value.vehicle_model}, license plate n°  ${Entry.value.vehicle_licenceplate}  ?')">Delete</a></td>
				
				
				
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
	<a href="<%=application.getContextPath()%>/addVehicle">Ajouter un véhicule</a>
	
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.20/js/dataTables.jqueryui.min.js"></script>
	
	<script>
	
	$(document).ready(function() {
	    $('#table').DataTable();
	} );
	
	</script>
	
</body>
</html>