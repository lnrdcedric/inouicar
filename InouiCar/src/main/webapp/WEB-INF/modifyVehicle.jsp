<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<%@  taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="header.jsp" />
<h1>Add Vehicle</h1>
<div id="update">
	<form method="post" action="modifyVehicle">
		<input id="vehicle_id" name="vehicle_id" type="number" class="inputChamp"
			placeholder="vehicle_id" value="${Vehicle.vehicle_id }" style="display:none"/>
			<br /> 
			<input id="fk_vehiclecategory_id" name="fk_vehiclecategory_id" type="number" class="inputChamp"
			placeholder="fk_vehiclecategory_id" value="${Vehicle.fk_vehiclecategory_id }"/>
			<br /> 
			<input id="vehicle_brand" name="vehicle_brand"
			type="text" class="inputChamp" placeholder="vehicle_brand*" value="${Vehicle.vehicle_brand }" />
			<br />
			<input id="vehicle_model" name="vehicle_model" type="text" class="inputChamp"
			placeholder="vehicle_model *" value="${Vehicle.vehicle_model }" />
			<br /> 
			<input id="vehicle_licenceplate" name="vehicle_licenceplate" type="text" class="inputChamp"
			placeholder="vehicle_licenceplate *" value="${Vehicle.vehicle_licenceplate }"/>
			<br /> 
			<input id="vehicle_status" name="vehicle_status" type="number" class="inputChamp"
			placeholder="vehicle_status *" value="${Vehicle.vehicle_status }" />
			<br /> 
			<input id="vehicle_insurancepolicy" name="vehicle_insurancepolicy" type="text" class="inputChamp"
			placeholder="vehicle_insurancepolicy *" value="${Vehicle.vehicle_insurancepolicy }"/>
			<br /> 
			<input id="vehicle_standarddailyrate" name="vehicle_standarddailyrate" type="text" class="inputChamp"
			placeholder="vehicle_standarddailyrate *" value="${Vehicle.vehicle_standarddailyrate }"/>
			<br /> 
			<input id="vehicle_numberofseats" name="vehicle_numberofseats" type="number" class="inputChamp"
			placeholder="vehicle_numberofseats *" value="${Vehicle.vehicle_numberofseats }"/>
			<br /> 
			<br /> 	
			<input type="submit" value="Mettre à jour le vehicule" class="submitBtn" />
	</form>
</div>
<div id="erreur">
	<p>Erreurs <br>
	${Erreur }
	</p>
</div>
<jsp:include page="footer.jsp" />