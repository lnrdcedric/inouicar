<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<jsp:include page="header.jsp" />
<a href="<%=application.getContextPath()%>/index">
	<img src="./img/logoSmall.png" alt="inouicar" width="80" height="80"/>
	</a>
<h1>Add new customer</h1>
<div id="ajout">
	<form method="post" action="ajouter">
			<input id="nom" name="nom" type="text" class="inputChamp"
			placeholder="Last Name *" />
			<br /> 
			<input id="prenom" name="prenom"
			type="text" class="inputChamp" placeholder="First Name *" />
			<br />
			<input id="email" name="email" type="text" class="inputChamp"
			placeholder="Email *" />
			<br /> 
			<input id="mdp" name="mdp"
			type="password" class="inputChamp" placeholder="Choose Password *" />
			<br /> 
			<input id="mdp2" name="mdp2"
			type="password" class="inputChamp" placeholder="Verify Password *" />
			<br />
			<br /> 	
			<input type="submit" value="Create account" class="submitBtn" />
	</form>
</div>
<div id="erreur">
	<p>Errors :  <br>
	${Erreur }
	</p>
</div>
<jsp:include page="footer.jsp" />