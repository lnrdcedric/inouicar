<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@  taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<jsp:include page="header.jsp" />
<a href="<%=application.getContextPath()%>/index">
	<img src="./img/logoSmall.png" alt="inouicar" width="80" height="80"/>
	</a>
<h1>Add Vehicle</h1>
<div id="ajout">
	<form method="post" action="addVehicle">
		<input id="fk_vehiclecategory_id" name="fk_vehiclecategory_id"
			type="number" class="inputChamp" placeholder="fk_vehiclecategory_id" value="${fk_vehiclecategory_id }"/>
		<br /> <input id="vehicle_brand" name="vehicle_brand" type="text"
			class="inputChamp" placeholder="vehicle_brand*" value="${vehicle_brand }" /> <br /> <input
			id="vehicle_model" name="vehicle_model" type="text"
			class="inputChamp" placeholder="vehicle_model *" value="${vehicle_model }" /> <br /> <input
			id="vehicle_licenceplate" name="vehicle_licenceplate" type="text"
			class="inputChamp" placeholder="vehicle_licenceplate *" value="${vehicle_licenceplate }" /> <br /> <input
			id="vehicle_status" name="vehicle_status" type="number"
			class="inputChamp" placeholder="vehicle_status *" value="${vehicle_status }" /> <br /> <input
			id="vehicle_insurancepolicy" name="vehicle_insurancepolicy"
			type="text" class="inputChamp"
			placeholder="vehicle_insurancepolicy *" value="${vehicle_insurancepolicy }"/> <br /> <input
			id="vehicle_standarddailyrate" name="vehicle_standarddailyrate"
			type="text" class="inputChamp"
			placeholder="vehicle_standarddailyrate *" value="${vehicle_standarddailyrate }" /> <br /> <input
			id="vehicle_numberofseats" name="vehicle_numberofseats" type="number"
			class="inputChamp" placeholder="vehicle_numberofseats *" value="${vehicle_numberofseats }" /> <br />
		<br /> <input type="submit" value="Submit new vehicle"
			class="submitBtn" />
	</form>
</div>
<div id="erreur">
	<c:if test="${not empty Erreur}">
		<p>Errors : ${Erreur}</p>
	</c:if>
</div>
<jsp:include page="footer.jsp" />