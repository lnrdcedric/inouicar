package fr.tnf.mspr2.groupeC.controllers;


import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
/**
 * Connexion à la base de données à l'aide d'un driver (jdbc)
 * @author maxime
 *
 */

public class ConnexionDatabase {
	/*
	 *  Informations de connexion, pour connecter l'application à la BDD
	 */
	private String BDD = "votreBaseDeDonnée";
	private String url = "jdbc:mysql://localhost:8889/" + BDD;
	private String user = "root";
	private String passwd = "root";
	private Connection myConn;
	
	public ConnexionDatabase(String bDD, String url, String user, String passwd) {
		super();
		BDD = bDD;
		this.url = url;
		this.user = user;
		this.passwd = passwd;
		
		/*
		 *  On vérifie bien que la connexion avec la base de données
		 *  s'effectue sans aucun problème.
		 */
		
		try {
		    Class.forName("com.mysql.jdbc.Driver");
		    // Connexion à la base de données
		    myConn = DriverManager.getConnection(url, user, passwd);
		    System.out.println("Connecté");
		    
		} catch (Exception e){
		    e.printStackTrace();
		    System.out.println("Erreur");
		    System.exit(0);
		}
		
	}

	// Récupération de la base de données
	public Connection getMyConn() {
		return myConn;
	}
	
	

	
}
