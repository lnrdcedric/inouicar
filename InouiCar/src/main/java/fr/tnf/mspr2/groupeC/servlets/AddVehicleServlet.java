package fr.tnf.mspr2.groupeC.servlets;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Connection;

import fr.tnf.mspr2.groupeC.controllers.ConnexionDatabase;
import fr.tnf.mspr2.groupeC.controllers.VehicleDao;
import fr.tnf.mspr2.groupeC.pojos.Vehicle;

/**
 * Servlet Permettant d'ajouter un véhicule à la BDD
 */
@WebServlet(name="/AddVehicleServlet",urlPatterns= {"/addVehicle"})
public class AddVehicleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddVehicleServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/addVehicle.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//Connexion à la BDD
		ConnexionDatabase conn = getDbConnexion(this.getServletContext());

		// Initialisation de lA DAO Véhicule
		VehicleDao vDao=getVDao(this.getServletContext(),conn);

		// Initialisation de la variable erreur
		String Erreur = "";
		String newLine = System.getProperty("line.separator");
		//Tests pour connaitre les champs Nuls obligatoires et afficher une erreur
		if (request.getParameter("fk_vehiclecategory_id").length()==0)
		{
			Erreur += "<br>"+" Please enter a fk_vehiclecategory_id";
		}
		else
		{
			request.setAttribute("fk_vehiclecategory_id", request.getParameter("fk_vehiclecategory_id"));
		}
		if (request.getParameter("vehicle_brand").length()==0)
		{
			Erreur += "<br>"+"Please enter a vehicle brand";
		}
		else
		{
			request.setAttribute("vehicle_brand", request.getParameter("vehicle_brand"));
		}
		if (request.getParameter("vehicle_model").length()==0)
		{
			Erreur += "<br>"+"Please enter a vehicle model";
		}
		else
		{
			request.setAttribute("vehicle_model", request.getParameter("vehicle_model"));
		}
		if (request.getParameter("vehicle_licenceplate").length()==0)
		{
			Erreur += "<br>"+"Please enter a vehicle licenceplate";
		}
		else
		{
			request.setAttribute("vehicle_licenceplate", request.getParameter("vehicle_licenceplate"));
		}
		if (request.getParameter("vehicle_status").length()==0)
		{
			Erreur += "<br>"+"Please enter a vehicle status";
		}
		else
		{
			request.setAttribute("vehicle_status", request.getParameter("vehicle_status"));
		}
		if (request.getParameter("vehicle_insurancepolicy").length()==0)
		{
			Erreur += "<br>"+"Please enter a vehicle insurance policy ";
		}
		else
		{
			request.setAttribute("vehicle_insurancepolicy", request.getParameter("vehicle_insurancepolicy"));
		}
		if (request.getParameter("vehicle_standarddailyrate").length()==0)
		{
			Erreur += "<br>"+"Please enter a vehicle standard daily rate";
		}
		else
		{
			try
			{
				Double.parseDouble(request.getParameter("vehicle_standarddailyrate"));
				request.setAttribute("vehicle_standarddailyrate", request.getParameter("vehicle_standarddailyrate"));
			}
			catch(NumberFormatException e)
			{
				Erreur += "<br>"+"Please enter a number with '.' separator for standard daily rate";
			}
			
		}
		if (request.getParameter("vehicle_numberofseats").length()==0)
		{
			Erreur += "<br>"+"Please enter a vehicle number of seats";
		}
		else
		{
			request.setAttribute("vehicle_numberofseats", request.getParameter("vehicle_numberofseats"));
		}

		if (Erreur.length()>0)
		{
			request.setAttribute("Erreur", Erreur);
			this.getServletContext().getRequestDispatcher("/WEB-INF/addVehicle.jsp").forward(request,response);
		}
		else {
			//Création du nouveau vehicule
			// Définition des attributs
			int my_vehicle_id =0;
			int my_fk_vehiclecategory_id=Integer.parseInt(request.getParameter("fk_vehiclecategory_id"));
			String my_vehicle_brand= request.getParameter("vehicle_brand");
			String my_vehicle_model = request.getParameter("vehicle_model");
			String my_vehicle_licenceplate = request.getParameter("vehicle_licenceplate");
			int my_vehicle_status= Integer.parseInt(request.getParameter("vehicle_status"));
			String my_vehicle_insurancepolicy= request.getParameter("vehicle_insurancepolicy");
			Double my_vehicle_standarddailyrate= Double.parseDouble(request.getParameter("vehicle_standarddailyrate"));
			int my_vehicle_numberofseats= Integer.parseInt(request.getParameter("vehicle_numberofseats"));

			//Instanciation du nouveau véhicule
			Vehicle myNewCar = new Vehicle(my_vehicle_id,my_fk_vehiclecategory_id,my_vehicle_brand,my_vehicle_model,
					my_vehicle_licenceplate,my_vehicle_status,
					my_vehicle_insurancepolicy,
					my_vehicle_standarddailyrate,my_vehicle_numberofseats);
			
			// Ajout du nouveau véhicule dans la BDD
			vDao.addVehicle(myNewCar);


			response.sendRedirect(request.getContextPath()+"/vehicleList");
		}
	}


	// Connexion à la BDD
	public ConnexionDatabase getDbConnexion(ServletContext context) {
		ConnexionDatabase currentConn=(ConnexionDatabase) context.getAttribute("MY-DbConn");
		if( currentConn==null) {
			String BDD = "inouicar";
			String url = "jdbc:mysql://localhost:8889/" + BDD;
			String user = "root";
			String passwd = "root";

			currentConn=new ConnexionDatabase(BDD,url,user,passwd);
			context.setAttribute("MY-DbConn", currentConn);
		}
		return currentConn;
	}

	// Connexion à la DAO véhicule
	public VehicleDao getVDao(ServletContext context,ConnexionDatabase myConn) {
		VehicleDao currentVDao=(VehicleDao) context.getAttribute("MY-VDao");
		if( currentVDao==null) {


			currentVDao=new VehicleDao(myConn.getMyConn());
			context.setAttribute("MY-VDao", currentVDao);
		}
		return currentVDao;
	}
}
