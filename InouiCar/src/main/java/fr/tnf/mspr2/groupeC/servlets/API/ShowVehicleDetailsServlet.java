package fr.tnf.mspr2.groupeC.servlets.API;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.tnf.mspr2.groupeC.controllers.ConnexionDatabase;
import fr.tnf.mspr2.groupeC.controllers.VehicleDao;

/**
 * Servlet permettant d'afficher les détails d'un véhicule en JSON
 */
@WebServlet(name="/ShowVehicleDetailsServlet",urlPatterns= {"/API/vehiculeList/*"})
public class ShowVehicleDetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShowVehicleDetailsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// COnnexion à la BDD
		ConnexionDatabase conn = getDbConnexion(this.getServletContext());
		// Connexion à la DAO des véhicules
		VehicleDao vDao=getVDao(this.getServletContext(),conn);

		// Création de la liste des véhicules
		Map<Integer,String> vehicleList = new HashMap<Integer,String>(); 

		//Recupere la liste des vehicules
		vehicleList=vDao.getVehicleListJson("tot");
		
		//Recupere l'id dans l'url
		String url = null;
		url = (String) request.getAttribute("javax.servlet.include.request_uri");
		url = url == null ? ((HttpServletRequest) request).getRequestURI() : url;
		
		String[] splitUrl = url.split("vehiculeList/");
		
		String id = splitUrl[1];
		//Recupere le vehicle en question sous forme de JSON
		String VehicleDetails = vDao.getVehicleJson(id);
		// Ajoute les détails à la requête
		request.setAttribute("Vehicle_Details_Json", VehicleDetails);
		
		// Rendu dans l'API du JSON détaillé
		this.getServletContext().getRequestDispatcher("/WEB-INF/showVehicleDetailsAPI.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
	}
	
	public ConnexionDatabase getDbConnexion(ServletContext context) {
		ConnexionDatabase currentConn=(ConnexionDatabase) context.getAttribute("MY-DbConn");
		if( currentConn==null) {
			String BDD = "inouicar";
			String url = "jdbc:mysql://localhost:8889/" + BDD;
			String user = "root";
			String passwd = "root";
			
			currentConn=new ConnexionDatabase(BDD,url,user,passwd);
			context.setAttribute("MY-DbConn", currentConn);
		}
		return currentConn;
	}

	public VehicleDao getVDao(ServletContext context,ConnexionDatabase myConn) {
		VehicleDao currentVDao=(VehicleDao) context.getAttribute("MY-VDao");
		if( currentVDao==null) {
			
			
			currentVDao=new VehicleDao(myConn.getMyConn());
			context.setAttribute("MY-VDao", currentVDao);
		}
		return currentVDao;
	}


}
