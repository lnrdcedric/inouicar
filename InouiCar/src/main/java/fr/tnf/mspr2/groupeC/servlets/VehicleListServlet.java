package fr.tnf.mspr2.groupeC.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.tnf.mspr2.groupeC.controllers.ConnexionDatabase;
import fr.tnf.mspr2.groupeC.controllers.VehicleDao;
import fr.tnf.mspr2.groupeC.pojos.Vehicle;

/**
 * Servlet permettant de récupérer la liste des véhicules
 */
@WebServlet(name="/VehicleListServlet",urlPatterns= {"/vehicleList"})
public class VehicleListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public VehicleListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Connexion à la BDD
		ConnexionDatabase conn = getDbConnexion(this.getServletContext());
		// Connexion à la liste des véhicules
		VehicleDao vDao=getVDao(this.getServletContext(),conn);

		// S'il y a un id de vehicule à supprimer
		if(request.getParameter("vehicleDel_id")!=null)
		{
			// On supprime le véhicule grâce à son ID
			vDao.deleteVehicle(request.getParameter("vehicleDel_id"));
			// Redirection vers la page liste des véhicules
			response.sendRedirect(request.getContextPath()+"/vehicleList");
		}
		else
		{
			// Création de la liste des véhicules
			Map<Integer,Vehicle> vehicleList = new HashMap<Integer,Vehicle>(); 

			//Recupere la liste des vehicules
			vehicleList=vDao.getVehicleList("tot");

			// Passage en paramètres de la requête des véhicules
			request.setAttribute("Vehicle_List", vehicleList);

			// Envoi de la requête à la JSP
			this.getServletContext().getRequestDispatcher("/WEB-INF/vehicleList.jsp").forward(request,response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	// Connexion à la BDD
	public ConnexionDatabase getDbConnexion(ServletContext context) {
		ConnexionDatabase currentConn=(ConnexionDatabase) context.getAttribute("MY-DbConn");
		if( currentConn==null) {
			String BDD = "inouicar";
			String url = "jdbc:mysql://localhost:8889/" + BDD;
			String user = "root";
			String passwd = "root";

			currentConn=new ConnexionDatabase(BDD,url,user,passwd);
			context.setAttribute("MY-DbConn", currentConn);
		}
		return currentConn;
	}

	// Connection à la DAO vehicules
	public VehicleDao getVDao(ServletContext context,ConnexionDatabase myConn) {
		VehicleDao currentVDao=(VehicleDao) context.getAttribute("MY-VDao");
		if( currentVDao==null) {


			currentVDao=new VehicleDao(myConn.getMyConn());
			context.setAttribute("MY-VDao", currentVDao);
		}
		return currentVDao;
	}

}
