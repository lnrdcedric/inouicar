package fr.tnf.mspr2.groupeC.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Date;

import fr.tnf.mspr2.groupeC.controllers.ConnexionDatabase;
import fr.tnf.mspr2.groupeC.pojos.Client;
import fr.tnf.mspr2.groupeC.controllers.ClientDao;

/**
 * Servlet implementation class Ajouter
 */
@WebServlet(name="/AjouterClient",urlPatterns= {"/addCustomer"})
public class AjouterClient extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjouterClient() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajouter.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		
		String BDD = "mstest";
		String url = "jdbc:mysql://localhost:8889/" + BDD;
		String user = "root";
		String passwd = "root";
		ConnexionDatabase conn = new ConnexionDatabase(BDD,url,user,passwd);
		
		ClientDao cDao=new ClientDao(conn.getMyConn());
		
		int new_customer_id = 0;
		String new_customer_lastname = request.getParameter("customer_lastname");
		String new_customer_firstname = request.getParameter("customer_firstname");
		int new_customer_login = Integer.parseInt(request.getParameter("customer_login"));
		String new_customer_password = request.getParameter("customer_password");
		Date new_customer_registrationdate = null;
		try {
			new_customer_registrationdate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("customer_registrationdate"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date new_customer_birthdate = null;
		try {
			new_customer_birthdate = new SimpleDateFormat("yyy-MM-dd").parse(request.getParameter("customer_birthdate"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String new_customer_drivinglicence = request.getParameter("customer_drivinglicence");
		int new_customer_gdprapproval = Integer.parseInt(request.getParameter("customer_gdprapproval"));
		Date new_customer_gdprapprovaldate = null;
		try {
			new_customer_gdprapprovaldate = new SimpleDateFormat("yyy-MM-dd").parse(request.getParameter("customer_lastname"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String new_mail = request.getParameter("mail");
		int new_customer_phonenumber = Integer.parseInt(request.getParameter("customer_phonenumber"));
		int new_customer_termsapproval = Integer.parseInt(request.getParameter("customer_termsapproval"));
		Date new_customer_termsapprovaldate = null;
		try {
			new_customer_termsapprovaldate = new SimpleDateFormat("yyy-MM-dd").parse(request.getParameter("customer_termsapprovaldate"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String new_customer_adress = request.getParameter("customer_adress");
		int new_customer_postcode = Integer.parseInt(request.getParameter("customer_postcode"));
		String new_customer_identitydocnumber = request.getParameter("customer_identitydocnumber");
		Date new_customer_forgetmedate = null;
		try {
			new_customer_forgetmedate = new SimpleDateFormat("yyy-MM-dd").parse(request.getParameter("customer_forgetmedate"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int new_customer_profile = Integer.parseInt(request.getParameter("customer_profile"));		
		
		
		
		Client newClient = new Client(new_customer_id, new_customer_lastname, new_customer_firstname, new_customer_login, new_customer_password, new_customer_registrationdate, new_customer_birthdate, new_customer_drivinglicence, new_customer_gdprapproval, new_customer_gdprapprovaldate, new_mail, new_customer_phonenumber, new_customer_termsapproval , new_customer_termsapprovaldate, new_customer_adress, new_customer_postcode , new_customer_identitydocnumber, new_customer_forgetmedate, new_customer_profile);
				
				
		cDao.addClient(newClient);
		
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/addCustomer.jsp").forward(request,response);
	}

}
