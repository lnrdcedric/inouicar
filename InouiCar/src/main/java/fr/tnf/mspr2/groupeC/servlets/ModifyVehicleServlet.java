package fr.tnf.mspr2.groupeC.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.tnf.mspr2.groupeC.controllers.ConnexionDatabase;
import fr.tnf.mspr2.groupeC.controllers.VehicleDao;
import fr.tnf.mspr2.groupeC.pojos.Vehicle;

/**
 * Servlet permettant la mise à jour d'un véhicule
 */
@WebServlet(name="/ModifyVehicleServlet",urlPatterns= {"/modifyVehicle"})
public class ModifyVehicleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifyVehicleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// S'il n'y a pas d'id en paramètre
		if(request.getParameter("vehicle_id")==null)
		{	
			// On redirige vers la liste des véhicules
			response.sendRedirect(request.getContextPath()+"/vehicleList");
		}
		//Sinon on récupère les données relatives au véhicle
		else
		{
			//Connexion BDD
			ConnexionDatabase conn = getDbConnexion(this.getServletContext());

			// Connexion DAO vehicule
			VehicleDao vDao=getVDao(this.getServletContext(),conn);
			
			// Récupération de l'ID
			int vehicle_id = Integer.parseInt(request.getParameter("vehicle_id"));
			
			// Création de la liste des véhicules
			Map<Integer,Vehicle> vehicleList = new HashMap<Integer,Vehicle>(); 

			//Recupere la liste des vehicules
			vehicleList=vDao.getVehicleList("tot");
			
			// Instanciation du véhicule à modifier
			Vehicle carToModify = vehicleList.get(vehicle_id);

			// Envoi des paramètres dans la requête
			request.setAttribute("Vehicle", carToModify);
			request.setAttribute("Vehicle_id", vehicle_id);
			// Redirection vers la JSP
			this.getServletContext().getRequestDispatcher("/WEB-INF/modifyVehicle.jsp").forward(request,response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Connection à la BDD
		ConnexionDatabase conn = getDbConnexion(this.getServletContext());
		// Connexion à la DAo véhicule
		VehicleDao vDao=getVDao(this.getServletContext(),conn);
		
		// Récupération des nouveaux attributs modifiés
		int my_vehicle_id =Integer.parseInt(request.getParameter("vehicle_id"));
		int my_fk_vehiclecategory_id=Integer.parseInt(request.getParameter("fk_vehiclecategory_id"));
		String my_vehicle_brand= request.getParameter("vehicle_brand");
		String my_vehicle_model = request.getParameter("vehicle_model");
		String my_vehicle_licenceplate = request.getParameter("vehicle_licenceplate");
		int my_vehicle_status= Integer.parseInt(request.getParameter("vehicle_status"));
		String my_vehicle_insurancepolicy= request.getParameter("vehicle_insurancepolicy");
		Double my_vehicle_standarddailyrate= Double.parseDouble(request.getParameter("vehicle_standarddailyrate"));
		int my_vehicle_numberofseats= Integer.parseInt(request.getParameter("vehicle_numberofseats"));
		
		//Instanciation du véhicule
		Vehicle myNewCar = new Vehicle(my_vehicle_id,my_fk_vehiclecategory_id,my_vehicle_brand,my_vehicle_model,
				my_vehicle_licenceplate,my_vehicle_status,
				my_vehicle_insurancepolicy,
				my_vehicle_standarddailyrate,my_vehicle_numberofseats);
		
		// Mise à jour du véhicule dans la BDD
		vDao.updateVehicle(myNewCar);
		
		// Redirection vers la JSP liste des véhicules
		response.sendRedirect(request.getContextPath()+"/vehicleList");
	}

	// Connexion à la BDD
	public ConnexionDatabase getDbConnexion(ServletContext context) {
		ConnexionDatabase currentConn=(ConnexionDatabase) context.getAttribute("MY-DbConn");
		if( currentConn==null) {
			String BDD = "inouicar";
			String url = "jdbc:mysql://localhost:8889/" + BDD;
			String user = "root";
			String passwd = "root";
			
			currentConn=new ConnexionDatabase(BDD,url,user,passwd);
			context.setAttribute("MY-DbConn", currentConn);
		}
		return currentConn;
	}
	
	// Connexion à la DAO des véhicules
	public VehicleDao getVDao(ServletContext context,ConnexionDatabase myConn) {
		VehicleDao currentVDao=(VehicleDao) context.getAttribute("MY-VDao");
		if( currentVDao==null) {
			
			
			currentVDao=new VehicleDao(myConn.getMyConn());
			context.setAttribute("MY-VDao", currentVDao);
		}
		return currentVDao;
	}
}

