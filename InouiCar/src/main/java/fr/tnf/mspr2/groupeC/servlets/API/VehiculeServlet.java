package fr.tnf.mspr2.groupeC.servlets.API;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.tnf.mspr2.groupeC.controllers.ConnexionDatabase;
import fr.tnf.mspr2.groupeC.controllers.VehicleDao;
import fr.tnf.mspr2.groupeC.pojos.Vehicle;

/**
 * Servlet permettant de renvoyer un JSON avec la liste des vehicules
 */
@WebServlet(name="/VehiculeServlet",urlPatterns= {"/API/vehiculeList"})
public class VehiculeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VehiculeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Connexion à la BDD
		ConnexionDatabase conn = getDbConnexion(this.getServletContext());
		// Connnexion à la DAO vehicules
		VehicleDao vDao=getVDao(this.getServletContext(),conn);
		//Création de la liste des véhicules
		Map<Integer,String> vehicleList = new HashMap<Integer,String>(); 
		
		//Recupere la liste des vehicules
		String vListBis= vDao.getVehicleListJsonBis();
		
		// Ajout des éléments à la requête
		request.setAttribute("Vehicle_List_JsonBis", vListBis);
	
		/*
		vehicleList=vDao.getVehicleListJson("tot");
		request.setAttribute("Vehicle_List_Json", vehicleList);*/
		
		// Envoi de la liste des véhicules à la JSP
		this.getServletContext().getRequestDispatcher("/WEB-INF/vehicleListAPI.jsp").forward(request,response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	
	public ConnexionDatabase getDbConnexion(ServletContext context) {
		ConnexionDatabase currentConn=(ConnexionDatabase) context.getAttribute("MY-DbConn");
		if( currentConn==null) {
			String BDD = "inouicar";
			String url = "jdbc:mysql://localhost:8889/" + BDD;
			String user = "root";
			String passwd = "root";
			
			currentConn=new ConnexionDatabase(BDD,url,user,passwd);
			context.setAttribute("MY-DbConn", currentConn);
		}
		return currentConn;
	}

	public VehicleDao getVDao(ServletContext context,ConnexionDatabase myConn) {
		VehicleDao currentVDao=(VehicleDao) context.getAttribute("MY-VDao");
		if( currentVDao==null) {
			
			
			currentVDao=new VehicleDao(myConn.getMyConn());
			context.setAttribute("MY-VDao", currentVDao);
		}
		return currentVDao;
	}


}
