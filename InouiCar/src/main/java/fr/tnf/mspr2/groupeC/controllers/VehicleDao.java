package fr.tnf.mspr2.groupeC.controllers;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.tnf.mspr2.groupeC.pojos.Vehicle;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

/**
 * DAO de connection à la table des véhicules
 * 
 */

public class VehicleDao 
{
	private Connection myConn;

	public VehicleDao(Connection conn) 
	{
		super();
		myConn=conn;
	}

	// Méthode pour ajouter un véhicule
	public void addVehicle(Vehicle myCar)
	{
		try {

			// Requête SQL permettant de récupérer la liste des véhicules
			
			String query = "INSERT INTO `vehicles` ("+myCar.getSqlQuery()+") VALUES (NULL,'"+myCar.getFk_vehiclecategory_id()+"','"+myCar.getVehicle_brand()+"', '"+myCar.getVehicle_model()+"', '"+ myCar.getVehicle_licenceplate()+"', '"+ myCar.getVehicle_status()+"', '"+ myCar.getVehicle_insurancepolicy()+"', '"+ myCar.getVehicle_standarddailyrate()+"','"+myCar.getVehicle_numberofseats()+"')";
			System.out.println(query);
			
			// Création de l'objet java
			Statement st = myConn.createStatement();

			// exécution de la requête sql
			int rs = st.executeUpdate(query);

			st.close();
			System.out.println("Vehicule Ajouté");
		} catch (Exception e){
			System.out.println("Vehicule non ajouté... erreur ");
			e.printStackTrace();
			System.out.println("Erreur");
			System.exit(0);
		}
	}

	// Méthode pour mettre à jour un véhicule de la BDD à partir de son ID (véhicule entier remplacé)
	public void updateVehicle(Vehicle myCar)
	{
		try {

			// Requête SQL permettant de récupérer la liste des véhicules
			String query = "UPDATE `vehicles` SET "+ myCar.getSqlQueryUpdate() +"  WHERE `vehicles`.`vehicle_id` ="+myCar.getVehicle_id();

			System.out.println(query);
			// Création de l'objet java
			Statement st = myConn.createStatement();

			// exécution de la requête sql
			int rs = st.executeUpdate(query);

			st.close();
			System.out.println("Vehicule Modifié");
		} catch (Exception e){
			System.out.println("Vehicule non modifié... erreur ");
			e.printStackTrace();
			System.out.println("Erreur");
			System.exit(0);
		}
	}

	// Méthode pour effacer un véhicule de la BDD à partir de son ID
	public void deleteVehicle(String myCarId)
	{
		try {

			// Requête SQL permettant de récupérer la liste des véhicules
			String query = "DELETE FROM `vehicles` WHERE `vehicles`.`vehicle_id` = "+myCarId;
			//String query = "INSERT INTO `vehicles` ("+myCar.getSqlQuery()+") VALUES (NULL,'"+myCar.getFk_vehiclecategory_id()+"','"+myCar.getVehicle_brand()+"', '"+myCar.getVehicle_model()+"', '"+ myCar.getVehicle_licenceplate()+"', '"+ myCar.getVehicle_status()+"', '"+ myCar.getVehicle_insurancepolicy()+"', '"+ myCar.getVehicle_standarddailyrate()+"','"+myCar.getVehicle_numberofseats()+"')";
			System.out.println(query);
			// Création de l'objet java
			Statement st = myConn.createStatement();

			// exécution de la requête sql
			int rs = st.executeUpdate(query);

			st.close();
			System.out.println("Vehicule Supprimé");
		} catch (Exception e){
			System.out.println("Vehicule non supprimé... erreur ");
			e.printStackTrace();
			System.out.println("Erreur");
			System.exit(0);
		}
	}

	// Méthode qui permet de récupérer la liste des véhicules
	public Map<Integer,Vehicle> getVehicleList(String myCarId)
	{
		// Creation de la liste des vehicules
		Map<Integer,Vehicle> vehicle_list = new HashMap<Integer,Vehicle>(); 

		try {

			// Requête SQL permettant de récupérer la liste des véhicules
			String query = "SELECT * FROM `vehicles`";


			// Création de l'objet java
			Statement st = myConn.createStatement();

			// exécution de la requête sql
			ResultSet rs = st.executeQuery(query);

			// Itération pour récupérer toutes les lignes de la requête
			while (rs.next())
			{

				int my_vehicle_id =rs.getInt("vehicle_id");
				int my_fk_vehiclecategory_id=rs.getInt("fk_vehiclecategory_id");
				String my_vehicle_brand= rs.getString("vehicle_brand");
				String my_vehicle_model = rs.getString("vehicle_model");
				String my_vehicle_licenceplate = rs.getString("vehicle_licenceplate");
				int my_vehicle_status= rs.getInt("vehicle_status");
				String my_vehicle_insurancepolicy= rs.getString("vehicle_insurancepolicy");
				Double my_vehicle_standarddailyrate= Double.parseDouble(rs.getString("vehicle_standarddailyrate"));
				int my_vehicle_numberofseats= rs.getInt("vehicle_numberofseats");

				Vehicle tempVehicle = new Vehicle(my_vehicle_id,my_fk_vehiclecategory_id,my_vehicle_brand,my_vehicle_model,
						my_vehicle_licenceplate,my_vehicle_status,
						my_vehicle_insurancepolicy,
						my_vehicle_standarddailyrate,my_vehicle_numberofseats);

				vehicle_list.put(my_vehicle_id, tempVehicle);
				
			}

			st.close();
			System.out.println("Liste de vehicules Créée");
		} catch (Exception e){
			System.out.println("Vehicle List not created");
			e.printStackTrace();
			System.out.println("Erreur");
			System.exit(0);
		}

		return vehicle_list;

	}

	// Méthode permettant de récupérer la liste des véhicules au format JSON (dans un objet java)
	public Map<Integer,String> getVehicleListJson(String myCarId)
	{
		// Creation de la liste des vehicules
		Map<Integer,String> vehicle_list = new HashMap<Integer,String>(); 

		try {

			// Requête SQL permettant de récupérer la liste des véhicules
			String query = "SELECT * FROM `vehicles`";


			// Création de l'objet java
			Statement st = myConn.createStatement();

			// exécution de la requête sql
			ResultSet rs = st.executeQuery(query);

			// Itération pour récupérer toutes les lignes de la requête
			while (rs.next())
			{

				int my_vehicle_id =rs.getInt("vehicle_id");
				int my_fk_vehiclecategory_id=rs.getInt("fk_vehiclecategory_id");
				String my_vehicle_brand= rs.getString("vehicle_brand");
				String my_vehicle_model = rs.getString("vehicle_model");
				String my_vehicle_licenceplate = rs.getString("vehicle_licenceplate");
				int my_vehicle_status= rs.getInt("vehicle_status");
				String my_vehicle_insurancepolicy= rs.getString("vehicle_insurancepolicy");
				Double my_vehicle_standarddailyrate= Double.parseDouble(rs.getString("vehicle_standarddailyrate"));
				int my_vehicle_numberofseats= rs.getInt("vehicle_numberofseats");

				Vehicle tempVehicle = new Vehicle(my_vehicle_id,my_fk_vehiclecategory_id,my_vehicle_brand,my_vehicle_model,
						my_vehicle_licenceplate,my_vehicle_status,
						my_vehicle_insurancepolicy,
						my_vehicle_standarddailyrate,my_vehicle_numberofseats);


				ObjectMapper mapper = new ObjectMapper();
				try {
					String jsonVehicle = mapper.writeValueAsString(tempVehicle);
					vehicle_list.put(my_vehicle_id, jsonVehicle);
					
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}

			}

			st.close();
			System.out.println("Liste de vehicules Créée");
		} catch (Exception e){
			System.out.println("Vehicle List not created");
			e.printStackTrace();
			System.out.println("Erreur");
			System.exit(0);
		}

		return vehicle_list;

	}

	// Méthode permettant de récupérer une liste des véhicules au format JSON (String)
	public String getVehicleListJsonBis()
	{
		// Creation de la liste des vehicules
		
		String vehicle_list="";
		
		List<Vehicle> vehicleListJson = new ArrayList() ;

		try {

			// Requête SQL permettant de récupérer la liste des véhicules
			String query = "SELECT * FROM `vehicles`";


			// création de l'objet java
			Statement st = myConn.createStatement();

			//exécution de la requête
			ResultSet rs = st.executeQuery(query);

			// itération sur les lignes résultant de la requête
			while (rs.next())
			{
				// Récupération des attributs du véhicule issus de la requête SQL
				int my_vehicle_id =rs.getInt("vehicle_id");
				int my_fk_vehiclecategory_id=rs.getInt("fk_vehiclecategory_id");
				String my_vehicle_brand= rs.getString("vehicle_brand");
				String my_vehicle_model = rs.getString("vehicle_model");
				String my_vehicle_licenceplate = rs.getString("vehicle_licenceplate");
				int my_vehicle_status= rs.getInt("vehicle_status");
				String my_vehicle_insurancepolicy= rs.getString("vehicle_insurancepolicy");
				Double my_vehicle_standarddailyrate= Double.parseDouble(rs.getString("vehicle_standarddailyrate"));
				int my_vehicle_numberofseats= rs.getInt("vehicle_numberofseats");

				//Instanciation d'un véhicule temporatire
				Vehicle tempVehicle = new Vehicle(my_vehicle_id,my_fk_vehiclecategory_id,my_vehicle_brand,my_vehicle_model,
						my_vehicle_licenceplate,my_vehicle_status,
						my_vehicle_insurancepolicy,
						my_vehicle_standarddailyrate,my_vehicle_numberofseats);

				// Ajout du véhicule à la liste temporaire
				vehicleListJson.add(tempVehicle);
			}

			st.close();

			// Création du JSON à partir de la liste des véhicules temporaire
			ObjectMapper mapper = new ObjectMapper();
			try {
				vehicle_list = mapper.writeValueAsString(vehicleListJson);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}

			// Ajout de l'en-tête pour le bon foncitonnement de l'API
			vehicle_list ="{\"vehicles\":" + vehicle_list + '}';
			System.out.println("Liste de vehicules Créée");
		} catch (Exception e){
			System.out.println("Vehicle List not created");
			e.printStackTrace();
			System.out.println("Erreur");
			System.exit(0);
		}

		// Retour d'un String contenant un JSON array avec tous les véhicules de la BDD
		return vehicle_list;

	}


	// Récupère un véhicle donné au format JSON à partir de son ID
	public String getVehicleJson(String VehicleId)
	{
		String vehicleDetails = "";


		try {

			// Requête SQL permettant de récupérer la liste des véhicules
			String query = "SELECT * FROM `vehicles` WHERE `vehicles`.`vehicle_id`= "+VehicleId;


			// Création de l'objet java
			Statement st = myConn.createStatement();

			// exécution de la requête sql
			ResultSet rs = st.executeQuery(query);

			// Itération pour récupérer toutes les lignes de la requête
			while (rs.next())
			{
				int my_vehicle_id =rs.getInt("vehicle_id");
				int my_fk_vehiclecategory_id=rs.getInt("fk_vehiclecategory_id");
				String my_vehicle_brand= rs.getString("vehicle_brand");
				String my_vehicle_model = rs.getString("vehicle_model");
				String my_vehicle_licenceplate = rs.getString("vehicle_licenceplate");
				int my_vehicle_status= rs.getInt("vehicle_status");
				String my_vehicle_insurancepolicy= rs.getString("vehicle_insurancepolicy");
				Double my_vehicle_standarddailyrate= Double.parseDouble(rs.getString("vehicle_standarddailyrate"));
				int my_vehicle_numberofseats= rs.getInt("vehicle_numberofseats");

				Vehicle tempVehicle = new Vehicle(my_vehicle_id,my_fk_vehiclecategory_id,my_vehicle_brand,my_vehicle_model,
						my_vehicle_licenceplate,my_vehicle_status,
						my_vehicle_insurancepolicy,
						my_vehicle_standarddailyrate,my_vehicle_numberofseats);



				ObjectMapper mapper = new ObjectMapper();
				try {
					vehicleDetails = mapper.writeValueAsString(tempVehicle);

				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
			}
			st.close();

		} catch (Exception e){
			System.out.println("Vehicle Details not created");
			e.printStackTrace();
			System.out.println("Erreur");
			System.exit(0);
		}

		return vehicleDetails;

	}


}
