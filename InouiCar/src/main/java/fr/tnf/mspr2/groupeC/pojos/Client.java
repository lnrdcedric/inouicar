package fr.tnf.mspr2.groupeC.pojos;

/** 
 * @author maxime
 * 
 * Classe Client permet de récupérer les données client depuis la BDD
 *
 */

import java.util.Date;
public class Client 
{
	private int customer_id ;
	private String customer_lastname;
	private String customer_firstname;
	private int customer_login;
	private String customer_password;
	private Date customer_registrationdate;
	private Date customer_birthdate;
	private String customer_drivinglicence;
	private int customer_gdprapproval;
	private Date customer_gdprapprovaldate;
	private String mail;
	private int customer_phonenumber;
	private int customer_termsapproval; 
	private Date customer_termsapprovaldate; 
	private String customer_adress;
	private int customer_postcode; 
	private String customer_identitydocnumber;
	private Date customer_forgetmedate;
	private int customer_profile;
	
	private String sqlQuery="`customer_id`,`customer_lastname`,`customer_firstname`,`customer_login`,`customer_password`,`customer_registrationdate`,`customer_birthdate`,`customer_drivinglicence`,`customer_gdprapproval`, `mail`,`customer_phonenumber`,`customer_termsapproval`,`customer_termsapprovaldate`,`customer_adress`, `customer_postcode`, `customer_identitydocnumber`, `customer_forgetmedate`, `customer_profile` ";
	
	public Client() {
		
	}
	
	

	public Client(int customer_id, String customer_lastname, String customer_firstname, int customer_login,
			String customer_password, Date customer_registrationdate, Date customer_birthdate,
			String customer_drivinglicence, int customer_gdprapproval, Date customer_gdprapprovaldate, String mail,
			int customer_phonenumber, int customer_termsapproval, Date customer_termsapprovaldate,
			String customer_adress, int customer_postcode, String customer_identitydocnumber,
			Date customer_forgetmedate, int customer_profile) {
		super();
		this.customer_id = customer_id;
		this.customer_lastname = customer_lastname;
		this.customer_firstname = customer_firstname;
		this.customer_login = customer_login;
		this.customer_password = customer_password;
		this.customer_registrationdate = customer_registrationdate;
		this.customer_birthdate = customer_birthdate;
		this.customer_drivinglicence = customer_drivinglicence;
		this.customer_gdprapproval = customer_gdprapproval;
		this.customer_gdprapprovaldate = customer_gdprapprovaldate;
		this.mail = mail;
		this.customer_phonenumber = customer_phonenumber;
		this.customer_termsapproval = customer_termsapproval;
		this.customer_termsapprovaldate = customer_termsapprovaldate;
		this.customer_adress = customer_adress;
		this.customer_postcode = customer_postcode;
		this.customer_identitydocnumber = customer_identitydocnumber;
		this.customer_forgetmedate = customer_forgetmedate;
		this.customer_profile = customer_profile;
	}



	public int getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_lastname() {
		return customer_lastname;
	}

	public void setCustomer_lastname(String customer_lastname) {
		this.customer_lastname = customer_lastname;
	}

	public String getCustomer_firstname() {
		return customer_firstname;
	}

	public void setCustomer_firstname(String customer_firstname) {
		this.customer_firstname = customer_firstname;
	}

	public int getCustomer_login() {
		return customer_login;
	}

	public void setCustomer_login(int customer_login) {
		this.customer_login = customer_login;
	}

	public String getCustomer_password() {
		return customer_password;
	}

	public void setCustomer_password(String customer_password) {
		this.customer_password = customer_password;
	}

	public Date getCustomer_registrationdate() {
		return customer_registrationdate;
	}

	public void setCustomer_registrationdate(Date customer_registrationdate) {
		this.customer_registrationdate = customer_registrationdate;
	}

	public Date getCustomer_birthdate() {
		return customer_birthdate;
	}

	public void setCustomer_birthdate(Date customer_birthdate) {
		this.customer_birthdate = customer_birthdate;
	}

	public String getCustomer_drivinglicence() {
		return customer_drivinglicence;
	}

	public void setCustomer_drivinglicence(String customer_drivinglicence) {
		this.customer_drivinglicence = customer_drivinglicence;
	}

	public int getCustomer_gdprapproval() {
		return customer_gdprapproval;
	}

	public void setCustomer_gdprapproval(int customer_gdprapproval) {
		this.customer_gdprapproval = customer_gdprapproval;
	}

	public Date getCustomer_gdprapprovaldate() {
		return customer_gdprapprovaldate;
	}

	public void setCustomer_gdprapprovaldate(Date customer_gdprapprovaldate) {
		this.customer_gdprapprovaldate = customer_gdprapprovaldate;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public int getCustomer_phonenumber() {
		return customer_phonenumber;
	}

	public void setCustomer_phonenumber(int customer_phonenumber) {
		this.customer_phonenumber = customer_phonenumber;
	}

	public int getCustomer_termsapproval() {
		return customer_termsapproval;
	}

	public void setCustomer_termsapproval(int customer_termsapproval) {
		this.customer_termsapproval = customer_termsapproval;
	}

	public Date getCustomer_termsapprovaldate() {
		return customer_termsapprovaldate;
	}

	public void setCustomer_termsapprovaldate(Date customer_termsapprovaldate) {
		this.customer_termsapprovaldate = customer_termsapprovaldate;
	}

	public String getCustomer_adress() {
		return customer_adress;
	}

	public void setCustomer_adress(String customer_adress) {
		this.customer_adress = customer_adress;
	}

	public int getCustomer_postcode() {
		return customer_postcode;
	}

	public void setCustomer_postcode(int customer_postcode) {
		this.customer_postcode = customer_postcode;
	}

	public String getCustomer_identitydocnumber() {
		return customer_identitydocnumber;
	}

	public void setCustomer_identitydocnumber(String customer_identitydocnumber) {
		this.customer_identitydocnumber = customer_identitydocnumber;
	}

	public Date getCustomer_forgetmedate() {
		return customer_forgetmedate;
	}

	public void setCustomer_forgetmedate(Date customer_forgetmedate) {
		this.customer_forgetmedate = customer_forgetmedate;
	}

	public int getCustomer_profile() {
		return customer_profile;
	}

	public void setCustomer_profile(int customer_profile) {
		this.customer_profile = customer_profile;
	}

	public String getSqlQuery() {
		return sqlQuery;
	}



	@Override
	public String toString() {
		return "Client [customer_id=" + customer_id + ", customer_lastname=" + customer_lastname
				+ ", customer_firstname=" + customer_firstname + ", customer_login=" + customer_login
				+ ", customer_password=" + customer_password + ", customer_registrationdate="
				+ customer_registrationdate + ", customer_birthdate=" + customer_birthdate
				+ ", customer_drivinglicence=" + customer_drivinglicence + ", customer_gdprapproval="
				+ customer_gdprapproval + ", customer_gdprapprovaldate=" + customer_gdprapprovaldate + ", mail=" + mail
				+ ", customer_phonenumber=" + customer_phonenumber + ", customer_termsapproval="
				+ customer_termsapproval + ", customer_termsapprovaldate=" + customer_termsapprovaldate
				+ ", customer_adress=" + customer_adress + ", customer_postcode=" + customer_postcode
				+ ", customer_identitydocnumber=" + customer_identitydocnumber + ", customer_forgetmedate="
				+ customer_forgetmedate + ", customer_profile=" + customer_profile + ", sqlQuery=" + sqlQuery + "]";
	}
	
	

	

	
}