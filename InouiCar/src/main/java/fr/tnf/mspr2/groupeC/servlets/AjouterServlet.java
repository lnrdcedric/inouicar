package fr.tnf.mspr2.groupeC.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.tnf.mspr2.groupeC.controllers.ConnexionDatabase;
import fr.tnf.mspr2.groupeC.controllers.CustomerDao;

/**
 * Servlet permettant d'ajouter un client
 */
@WebServlet(name="/AjouterServlet",urlPatterns= {"/ajouter"})
public class AjouterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjouterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajouter.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Récupère les informations du client
		String client_surname = (String) request.getParameter("nom");
		String client_name = (String) request.getParameter("prenom");
		
		// Connexion à la BDD
		String BDD = "mstest";
		String url = "jdbc:mysql://localhost:8889/" + BDD;
		String user = "root";
		String passwd = "root";
		ConnexionDatabase conn = new ConnexionDatabase(BDD,url,user,passwd);
		
		//Connexion à la DAO
		CustomerDao cDao=new CustomerDao(conn.getMyConn());
		// Ajout d'un Client
		cDao.addCustomer(client_name, client_surname);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajouter.jsp").forward(request,response);
	}

}
