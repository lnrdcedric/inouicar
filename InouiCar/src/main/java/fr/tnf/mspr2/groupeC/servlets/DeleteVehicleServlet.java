package fr.tnf.mspr2.groupeC.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.tnf.mspr2.groupeC.controllers.ConnexionDatabase;
import fr.tnf.mspr2.groupeC.controllers.VehicleDao;
import fr.tnf.mspr2.groupeC.pojos.Vehicle;

/**
 * Servlet permettant de supprimer un véhicule
 */
@WebServlet(name="/DeleteVehicleServlet",urlPatterns= {"/deleteVehicle"})
public class DeleteVehicleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteVehicleServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		// Connexion à la BDD
		ConnexionDatabase conn = getDbConnexion(this.getServletContext());
		
		// COnnexion à la DAO véhicule
		VehicleDao vDao=getVDao(this.getServletContext(),conn);
		
		// Si il y a bien un id de véhicule est passé en paramètre
		if(request.getParameter("vehicleDel_id")!=null)
		{
			// On supprime le véhicule correspondant à cet ID
			vDao.deleteVehicle(request.getParameter("vehicleDel_id"));
		}
		
		
		Map<Integer,Vehicle> vehicleList = new HashMap<Integer,Vehicle>(); 
		
		//Recupere la liste des vehicules
		vehicleList=vDao.getVehicleList("tot");
		
		request.setAttribute("Vehicle_List", vehicleList);
		
		// Redirection vers la liste des véhicules
		this.getServletContext().getRequestDispatcher("/WEB-INF/vehicleList.jsp").forward(request,response);
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/WEB-INF/deleteVehicle.jsp").forward(request,response);
	}
	
	// Connexion à la BDD
	public ConnexionDatabase getDbConnexion(ServletContext context) {
		ConnexionDatabase currentConn=(ConnexionDatabase) context.getAttribute("MY-DbConn");
		if( currentConn==null) {
			String BDD = "inouicar";
			String url = "jdbc:mysql://localhost:8889/" + BDD;
			String user = "root";
			String passwd = "root";
			
			currentConn=new ConnexionDatabase(BDD,url,user,passwd);
			context.setAttribute("MY-DbConn", currentConn);
		}
		return currentConn;
	}

	// Connexion à la DAO véhicule
	public VehicleDao getVDao(ServletContext context,ConnexionDatabase myConn) {
		VehicleDao currentVDao=(VehicleDao) context.getAttribute("MY-VDao");
		if( currentVDao==null) {
			
			
			currentVDao=new VehicleDao(myConn.getMyConn());
			context.setAttribute("MY-VDao", currentVDao);
		}
		return currentVDao;
	}

}
