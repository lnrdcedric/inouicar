package fr.tnf.mspr2.groupeC.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.tnf.mspr2.groupeC.controllers.ConnexionDatabase;

/**
 * Servlet permettant l'enregistrement d'un client dans la BDD  
 */
@WebServlet(name="/InscriptionServlet",urlPatterns= {"/inscription"})
public class InscriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InscriptionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Connexion à la BDD
		String BDD = "mstest";
		String url = "jdbc:mysql://localhost:8889/" + BDD;
		String user = "root";
		String passwd = "root";
		ConnexionDatabase conn = new ConnexionDatabase(BDD,url,user,passwd);
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
