package fr.tnf.mspr2.groupeC.controllers;

import java.sql.*;

/**
 *  Dao permettant de connecter à la base de données table client
 *
 */

public class CustomerDao 
{
	private Connection myConn;
	
	public CustomerDao(Connection conn) 
	{
		super();
		myConn=conn;
	}
	
	public void addCustomer(String cName, String cLastName)
	{
		try {
		    
			// Requête SQL permettant de récupérer la liste des clients
				     
		      String query = "INSERT INTO `client` (`client_name`, `client_surname`, `client_id`) VALUES ('"+cName+"', '"+cLastName+"', NULL)";

		      // Création de l'objet java 
		      Statement st = myConn.createStatement();
		      
		      // Exécution de la requête sql
		      int rs = st.executeUpdate(query);
		      
		     // Fermeture de l'objet java
		      st.close();
		      System.out.println("Client Ajouté");
		} catch (Exception e){
			System.out.println("Client pas ajouté erreur ");
		    e.printStackTrace();
		    System.out.println("Erreur");
		    System.exit(0);
		} 
	}
}
