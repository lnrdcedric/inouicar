package fr.tnf.mspr2.groupeC.controllers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import fr.tnf.mspr2.groupeC.pojos.Client;

public class ClientDao {

private Connection myConn;
	
	public ClientDao(Connection conn) 
	{
		super();
		myConn=conn;
	}
	
	public void addClient(Client client)
	{
	
			try {
		    
		    // our SQL SELECT query. 
		      // if you only need a few columns, specify them by name instead of using "*/
				     
		      String query = "INSERT INTO `customers` ("+client.getSqlQuery()+") VALUES (NULL,'"+ client.getCustomer_lastname()+"','"+client.getCustomer_firstname()+"', '"+client.getCustomer_login()+"', '"+ client.getCustomer_password()+"', '"+ client.getCustomer_registrationdate()+"', '"+ client.getCustomer_birthdate()+"', '"+ client.getCustomer_drivinglicence()+"','"+client.getCustomer_gdprapproval()+"','"+client.getCustomer_gdprapprovaldate()+"','"+client.getMail()+"','"+client.getCustomer_phonenumber()+"','"+client.getCustomer_termsapproval()+"','"+client.getCustomer_termsapprovaldate()+"','"+client.getCustomer_adress()+"','"+client.getCustomer_postcode()+"','"+client.getCustomer_identitydocnumber()+"', '"+client.getCustomer_forgetmedate()+"', '"+client.getCustomer_profile()+"')";
		      System.out.println(query);
		      // create the java statement
		      Statement st = myConn.createStatement();
		      
		      // execute the query, and get a java resultset
		      int rs = st.executeUpdate(query);
		      
		      st.close();
		      System.out.println("Client Ajouté");
		} catch (Exception e){
			System.out.println("Client non ajouté... erreur ");
		    e.printStackTrace();
		    System.out.println("Erreur");
		    System.exit(0);
		}
	}
	
	public void deleteClient(String clientID)
	{
		try {
		    
		    // our SQL SELECT query. 
		      // if you only need a few columns, specify them by name instead of using "*/
				     
			String query = "DELETE FROM `customers` WHERE `customers`.`customer_id` = "+clientID;
		      //String query = "INSERT INTO `customers` ("+client.getSqlQuery()+") VALUES ("+client.getSqlQuery()+") VALUES (NULL,'"+ client.getCustomer_lastname()+"','"+client.getCustomer_firstname()+"', '"+client.getCustomer_login()+"', '"+ client.getCustomer_password()+"', '"+ client.getCustomer_registrationdate()+"', '"+ client.getCustomer_birthdate()+"', '"+ client.getCustomer_drivinglicence()+"','"+client.getCustomer_gdprapproval()+"','"+client.getCustomer_gdprapprovaldate()+"','"+client.getMail()+"','"+client.getCustomer_phonenumber()+"','"+client.getCustomer_termsapproval()+"','"+client.getCustomer_termsapprovaldate()+"','"+client.getCustomer_adress()+"','"+client.getCustomer_postcode()+"','"+client.getCustomer_identitydocnumber()+"', '"+client.getCustomer_forgetmedate()+"', '"+client.getCustomer_profile()+"')";
		      System.out.println(query);
		      // create the java statement
		      Statement st = myConn.createStatement();
		      
		      // execute the query, and get a java resultset
		      int rs = st.executeUpdate(query);
		      
		      st.close();
		      System.out.println("Client Supprimé");
		} catch (Exception e){
			System.out.println("Client non supprimé... erreur ");
		    e.printStackTrace();
		    System.out.println("Erreur");
		    System.exit(0);
		}
	}
	
	
	public void getClientList(String clientID)
	{
		try {
		    
		    // our SQL SELECT query. 
		      // if you only need a few columns, specify them by name instead of using "*/
				     
		      String query = "SELECT * FROM `customers`";
		      
		      
		      // create the java statement
		      Statement st = myConn.createStatement();
		      
		      // execute the query, and get a java resultset
		      ResultSet rs = st.executeQuery(query);
		      
		      st.close();
		      System.out.println("Client Ajouté");
		} catch (Exception e){
			System.out.println("Client non ajouté... erreur ");
		    e.printStackTrace();
		    System.out.println("Erreur");
		    System.exit(0);
		}
	}
	
	
	
	
	
}
